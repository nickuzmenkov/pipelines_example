import pytz

from pipeline_example.app import utc_now


def test_utc_now():
    now = utc_now()
    assert now.tzinfo == pytz.UTC
